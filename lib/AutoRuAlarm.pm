package AutoRuAlarm;
use Mojo::Base 'Mojolicious';

use utf8;

use AutoRuAlarm::Model;

# This method will run once at server start
sub startup {
	my $self = shift;

	# Documentation browser under "/perldoc"
	$self->plugin('PODRenderer');

	my $r = $self->routes;

	$r->get('/')->to(controller => 'common', action => 'home')->name('home');
	$r->get('/about')->to(controller => 'common', action => 'about')->name('about');
	$r->get('/contacts')->to(controller => 'common', action => 'contacts')->name('contacts');
	$r->post('/login')->to(controller => 'common', action => 'login');
	$r->get('/logout')->to(controller => 'common', action => 'logout')->name('logout');
	$r->get('/citylist')->to(controller => 'common', action => 'citylist');
	$r->get('/modellist')->to(controller => 'common', action => 'modellist');
	
	$r->get('/registration')->to(controller => 'registration', action => 'regform')->name('registration');
	$r->post('/registration')->to(controller => 'registration', action => 'register');
	$r->get('/registration/confirm')->to(controller => 'registration', action => 'confirm');
	$r->get('/registration/checkphone')->to(controller => 'registration', action => 'checkphone');

	$r->get('/account')->to(controller => 'registration', action => 'account')->name('account');
	$r->post('/account')->to(controller => 'registration', action => 'save_account');

	$r->get('/query')->to(controller => 'query', action => 'list')->name('querylist');
	$r->get('/archive')->to(controller => 'query', action => 'list', archive => 'archive')->name('queryarchivelist');

	$r->post('/query')->to(controller => 'query', action => 'manage');
	$r->post('/archive')->to(controller => 'query', action => 'manage');

	$r->get('/query/add')->to(controller => 'query', action => 'add')->name('queryadd');
	$r->post('/query/add')->to(controller => 'query', action => 'insert');

	$self->helper(
		model => sub { state $model = AutoRuAlarm::Model->init() }
	);
		
	$self->helper(
		dbh => sub { return $self->model->db() }
	  );
	
	$self->helper(
		settings => sub {
			my $self = shift;
			my $s_code = shift;
			my ($s_value) = $self->dbh->selectrow_array('select s_value from settings where s_code=?',undef,$s_code);
			return $s_value;
		}
	);
  
	$self->defaults(layout => 'default');
  
	$self->hook(
		before_dispatch => sub {
			my $self = shift;
			# notice: url must be fully-qualified or absolute, ending in '/' matters.
			$self->req->url->base(Mojo::URL->new(qq[http://$ENV{SERVER_NAME}/]));
		}
	);

}

1;
