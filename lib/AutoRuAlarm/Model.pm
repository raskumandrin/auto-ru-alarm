package AutoRuAlarm::Model;
use strict;
use warnings;

use DBI;
use utf8;

use Mojo::UserAgent;
use feature 'say';

my $dbh;

sub init {
	$dbh = DBI->connect("DBI:mysql:$ENV{ARA_DB}:$ENV{ARA_DB_HOST}:$ENV{ARA_DB_PORT}",$ENV{ARA_DB_LOGIN},$ENV{ARA_DB_PASS},{mysql_enable_utf8 => 1,PrintError => 0, RaiseError => 1});
	$dbh->do("set character set utf8");
	$dbh->do("set names utf8");
	$dbh->do("SET time_zone = 'Europe/Moscow'");

	bless {};
}

sub db {
	my ($self) = @_;
    return $dbh if $dbh;
    die "You should init model first!";
}


sub settings {
	my ($self,$code) = @_;
	my ($value) =  $dbh->selectrow_array( 'select s_value from settings where s_code=?',undef,$code );
	return $value;
}

1;
