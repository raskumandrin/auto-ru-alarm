package AutoRuAlarm::Registration;
use Mojo::Base 'Mojolicious::Controller';
use Digest::MD5 qw(md5_hex);
use Mojo::UserAgent;
use URI::Escape;

sub phone_hash_code {
	my $phone = shift;

	return substr(md5_hex($phone.'pych-pych ololo'),0,4);
}

sub regform {
	my $self = shift;
	
	$self->stash(
		region_list => $self->dbh->selectall_arrayref('select * from `region` order by r_name', { Slice => {} } ),
	);
		
	$self->render('registration/regform');
}

sub register {
	my $self = shift;

	my @errors;
	push @errors,'Не указано имя' unless $self->param('name');
	
	my $phone = $self->param('phone_1') . $self->param('phone_2') . $self->param('phone_3') . $self->param('phone_4');
	
	push @errors,'Некорректно указан телефон' unless $phone =~ /^\d{10}$/ ;
	push @errors,'Не указан пароль' unless $self->param('password');
	push @errors,'Не указан город' unless $self->param('city');

	push @errors,'Код подтверждения некорректен' if $self->param('code') ne phone_hash_code($phone);

	if (@errors) {
		my $error_list;
		$error_list .= "<li>$_</li>" foreach (@errors);

		$self->flash(alert_error => "<p>Регистрацию не удалось осуществить.</p><ul>$error_list</ul><p>При возникновении вопросов обратитесь пожалуйста в поддержку.</p>" );
		$self->redirect_to('home');
	}
	else {
		$self->dbh->do("insert into person (p_city,p_phone,p_name,p_password,p_mail) values (?,?,?,?,?)",undef
			,$self->param('city')
			,$phone
			,$self->param('name')
			,$self->param('password')
			,$self->param('mail')
		);

		$self->flash(alert_success => 'Вы успешно зарегистрировались' );
		$self->session(phone => $phone);
		$self->redirect_to('home');
	}
	
}

sub confirm {
	my $self = shift;
		
	my $text = uri_escape_utf8( 'Код подтверждения регистрации: '.phone_hash_code($self->param('phone')) );
	my $url = "http://bytehand.com:3800/send?id=1023\&key=61CE476D6E71AB23\&to=7".$self->param('phone')."\&from=SMS-INFO\&text=$text";
	
	my $ua = Mojo::UserAgent->new;
	$self->render(json => $ua->get($url)->res->body);
}


sub checkphone {
	my $self = shift;
	
	$self->render(json => {phone_exists =>
		$self->dbh->selectrow_array( 'select count(*) from person where p_phone=?',undef,$self->param('phone') )
	} );
}


sub account {
	my $self = shift;
	my ($ph1,$ph2,$ph3,$ph4) = ( $self->session('phone') =~ /^(\d{3})(\d{3})(\d{2})(\d{2})/ );
	$self->stash(
		name => $self->dbh->selectrow_array( 'select p_name from person where p_phone=?',undef,$self->session('phone') ),
		phone1 => $ph1,
		phone2 => $ph2,
		phone3 => $ph3,
		phone4 => $ph4,
	);
		
	$self->render('registration/account');
}


sub save_account {
	my $self = shift;

	my @errors;
	push @errors,'Не указано имя' unless $self->param('name');
	
	my $phone = $self->param('phone_1') . $self->param('phone_2') . $self->param('phone_3') . $self->param('phone_4');
	
	push @errors,'Некорректно указан телефон' unless $phone =~ /^\d{10}$/ ;

	push @errors,'Код подтверждения некорректен' if $self->param('code') ne phone_hash_code($phone);

	if (@errors) {
		my $error_list;
		$error_list .= "<li>$_</li>" foreach (@errors);

		$self->flash(alert_error => "<p>Не сохранено.</p><ul>$error_list</ul><p>При возникновении вопросов обратитесь пожалуйста в поддержку.</p>" );
		$self->redirect_to('account');
	}
	else {
		$self->dbh->do("update person set p_name=?,p_phone=? where p_phone=?",undef
			,$self->param('name')
			,$phone
			,$self->session('phone')
		);
		$self->dbh->do("update query set q_phone=? where q_phone=?",undef
			,$phone
			,$self->session('phone')
		);

		$self->flash(alert_success => 'Сохранено' );
		$self->session(phone => $phone);
		$self->redirect_to('account');
	}
	
}

1;
