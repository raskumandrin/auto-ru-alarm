package AutoRuAlarm::Common;
use Mojo::Base 'Mojolicious::Controller';

sub home {
	my $self = shift;
	$self->render('common/home');
}


sub contacts {
	my $self = shift;
	$self->render('common/contacts');
}


sub about {
	my $self = shift;
	$self->render('common/about');
}

sub login {
	my $self = shift;
	
	my $phone = $self->param('phone_1') . $self->param('phone_2') . $self->param('phone_3') . $self->param('phone_4');
	
	my $login_ok = 
		$self->dbh->selectrow_array( 'select count(*) from person where p_phone=? and p_password=?',undef
			,$phone
			,$self->param('password')
		);
		
	if ( $login_ok ) {
		$self->flash(alert_success => 'Вы успешно авторизовались' );
		$self->session(phone => $phone);
		$self->redirect_to('home');
	}
	else {
		$self->flash(alert_error => 'Авторизация не произведена' );
		$self->redirect_to('home');
	}
}

sub logout {
	my $self = shift;
	delete $self->session->{phone};
	$self->redirect_to('home');
}


sub citylist {
	my $self = shift;
	
	$self->render(json => 
		$self->dbh->selectall_arrayref('select c_id,c_name from `city` where c_region=? order by c_name', { Slice => {} }, $self->param('region') )
	);
}


sub modellist {
	my $self = shift;
	
	$self->render(json => 
		$self->dbh->selectall_arrayref('select m_id,m_name from `model` where m_brand=? order by m_name', { Slice => {} }, $self->param('brand') )
	);
}


1;
