package AutoRuAlarm::Region;
use Mojo::Base 'Mojolicious::Controller';
use utf8;

sub citylist {
	my $self = shift;
	
#	$self->stash(
#		city_list => $self->dbh->selectall_arrayref('select * from `city` where c_region=? order by c_name', { Slice => {} }, $self->param('region') ),
#	);
		$self->render(json => 
			$self->dbh->selectall_arrayref('select c_id,c_name from `city` where c_region=? order by c_name', { Slice => {} }, $self->param('region') )
		);
#	$self->render('region/citylist');
}

1;
