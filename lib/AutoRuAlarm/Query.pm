package AutoRuAlarm::Query;
use Mojo::Base 'Mojolicious::Controller';
use utf8;

sub list {
	my $self = shift;
	my $q_active = $self->stash('archive') ? 1 : 0;
##
## Разобраться с дублированием записей при join-объединениях
##
	my $query_list_sql = << 'EOF';
select distinct
	q_id
	,q_year_min,q_year_max
	,coalesce(r_name,'Все') as r_name
	,coalesce(c_name,'Все') as c_name
	,coalesce(b_name,'Все') as b_name
	,coalesce(m_name,'Все') as m_name

from query
left join region on r_id=q_region
left join city on c_id=q_city
left join brand on b_id=q_brand
left join model on m_id=q_model
where
	q_active=?
	and q_phone=?
order by q_creation_time desc
	
	
EOF
	
	$self->stash(
		query_list => $self->dbh->selectall_arrayref($query_list_sql, { Slice => {} }, 1-$q_active, $self->session('phone') ),
		archive => $q_active,
	);

	$self->render('query/list');
}

sub add {
	my $self = shift;
	
	$self->stash(
		region_list => $self->dbh->selectall_arrayref('select * from `region` order by r_name', { Slice => {} } ),
		brand_list => $self->dbh->selectall_arrayref('select * from `brand` order by b_name', { Slice => {} } ),
	);

	$self->render('query/add');
}


sub insert {
	my $self = shift;

	$self->dbh->do("INSERT INTO `query` (q_active,q_creation_time,q_phone,q_region,q_city,q_brand,q_model,q_year_min,q_year_max)
		VALUES (1,now(),?,?,?,?,?,?,?)",undef
		,$self->session('phone')
	  	,$self->param('region')
	  	,$self->param('city') || 0
		,$self->param('brand') || 0
		,$self->param('model') || 0
		,$self->param('year_min')
		,$self->param('year_max')
	);
	
	$self->flash(alert_success => 'Данные успешно добавлены' );
	
	$self->res->code(201);
	$self->redirect_to('/query');
}


sub manage {
	my $self = shift;

	foreach my $query_id ( grep(/^q\d/, $self->param() ) ) {
		($query_id) = ( $query_id =~ /(\d+)/ );
		
		if ( $self->param('delete') ) {
			$self->dbh->do('DELETE  FROM `query` where q_id=? and q_phone=?',undef,$query_id,$self->session('phone') );
		}

		if ( $self->param('archive') ) {
			$self->dbh->do('UPDATE `query` set q_active=0 where q_id=? and q_phone=?',undef,$query_id,$self->session('phone') );
		}
		
		if ( $self->param('unarchive') ) {
			$self->dbh->do('UPDATE `query` set q_active=1 where q_id=? and q_phone=?',undef,$query_id,$self->session('phone') );
		}
		
	}
	
	$self->flash(alert_success => 'Информация обновлена' );
	
#	$self->res->code(201);
	$self->redirect_to('/query');
}

1;
