/* включения группы инпутов для номера телефона */

$('.phone_input').groupinputs();

window.setTimeout(function() {
    $(".alert-auto-close").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 2000);

/* подгрузка городов по региону */

$('select[name="region"]').change(function(){

	$('select[name="city"]').empty();

	if ($('form.query-form').length > 0) {
		$('select[name="city"]').append('<option>Все</option>');
	}
	else {
		$('select[name="city"]').append('<option></option>');
	}

	$.ajax({
		dataType: "json",
		url: '/citylist',
		data: { region : $('select[name="region"]').val() },
		async : false,
		success: function(data) {
			$.each(data, function(city) {
				$('select[name="city"]').append($("<option />").val(data[city].c_id).text(data[city].c_name));
			});
		}
	});

	$('div.region-city').slideDown();
	
});

/* подгрузка марок по моделям */

$('select[name="brand"]').change(function(){

	$('select[name="model"]').empty();


	$('select[name="model"]').append('<option>Все</option>');

	$.ajax({
		dataType: "json",
		url: '/modellist',
		data: { brand : $('select[name="brand"]').val() },
		async : false,
		success: function(data) {
			$.each(data, function(model) {
				$('select[name="model"]').append($("<option />").val(data[model].m_id).text(data[model].m_name));
			});
		}
	});

});

/* проверка формы регистрации */

function checkRegForm() {
	var submit = true;
	
	if ( $('input[name="name"]').val()=='' ) {
		$('input[name="name"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		$('input[name="name"]').siblings('span.help-inline').hide();
	}

	if ( $('input[name="phone_1"]').val()=='' || $('input[name="phone_2"]').val()=='' || $('input[name="phone_3"]').val()=='' || $('input[name="phone_4"]').val()=='') {
		
		
		$('input[name="phone_1"]').siblings('span.help-inline').text('Укажите трёхзначный код и номер телефона')
		$('input[name="phone_1"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		
		$.ajax({
			dataType: "json",
			url: '/registration/checkphone',
			data: { phone : '' +
				+$('input[name="phone_1"]').val()
				+$('input[name="phone_2"]').val()
				+$('input[name="phone_3"]').val()
				+$('input[name="phone_4"]').val()
			},
			async : false,
			success: function(data) {
				if (data.phone_exists == 1) {
					$('input[name="phone_1"]').siblings('span.help-inline').text('Такой номер уже зарегистрирован в системе')
					$('input[name="phone_1"]').siblings('span.help-inline').show();
					submit = false;
				}
				else {
					$('input[name="phone_1"]').siblings('span.help-inline').hide();
				}
			}
		});
	}

	if ( $('input[name="password"]').val()=='' ) {
		$('input[name="password"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		$('input[name="password"]').siblings('span.help-inline').hide();
	}

	if ( $('select[name="region"]').val()=='' ) {
		$('select[name="region"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		$('select[name="region"]').siblings('span.help-inline').hide();
	}

	if ( $('select[name="city"]').val()=='' && $('select[name="region"]').val()!='' ) {
		$('select[name="city"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		$('select[name="city"]').siblings('span.help-inline').hide();
	}

	return submit;
};


/* проверка формы account */

function checkAccForm() {
	var submit = true;
	
	if ( $('input[name="name"]').val()=='' ) {
		$('input[name="name"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		$('input[name="name"]').siblings('span.help-inline').hide();
	}

	if ( $('input[name="phone_1"]').val()=='' || $('input[name="phone_2"]').val()=='' || $('input[name="phone_3"]').val()=='' || $('input[name="phone_4"]').val()=='') {
		
		
		$('input[name="phone_1"]').siblings('span.help-inline').text('Укажите трёхзначный код и номер телефона')
		$('input[name="phone_1"]').siblings('span.help-inline').show();
		submit = false;
	}
	else {
		
		$.ajax({
			dataType: "json",
			url: '/registration/checkphone',
			data: { phone : '' +
				+$('input[name="phone_1"]').val()
				+$('input[name="phone_2"]').val()
				+$('input[name="phone_3"]').val()
				+$('input[name="phone_4"]').val()
			},
			async : false,
			success: function(data) {
				if (data.phone_exists == 1) {
					$('input[name="phone_1"]').siblings('span.help-inline').text('Такой номер уже зарегистрирован в системе')
					$('input[name="phone_1"]').siblings('span.help-inline').show();
					submit = false;
				}
				else {
					$('input[name="phone_1"]').siblings('span.help-inline').hide();
				}
			}
		});
	}

	return submit;
};
$('button.reg1').click(function(){
	if ( checkRegForm() ) {
		$(this).addClass('disabled');
		$('div.reg-part2').slideDown();

		$.get('/registration/confirm?phone='
			+$('input[name="phone_1"]').val()
			+$('input[name="phone_2"]').val()
			+$('input[name="phone_3"]').val()
			+$('input[name="phone_4"]').val()
		);
	}
	return false;
});


$('button.acc1').click(function(){
	if ( checkAccForm() ) {
		$(this).addClass('disabled');
		$('div.reg-part2').slideDown();

		$.get('/registration/confirm?phone='
			+$('input[name="phone_1"]').val()
			+$('input[name="phone_2"]').val()
			+$('input[name="phone_3"]').val()
			+$('input[name="phone_4"]').val()
		);
	}
	return false;
});