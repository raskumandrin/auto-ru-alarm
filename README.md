Смс-информер об объявлениях auto.ru 
=======================


Функции
-------

* Отслеживание объявлений на предмет появления требуемых параметров (регион, марка, год)
* Уведомления по смс и почте о произошедших изменениях

Установка
-------

Параметры базы данных предполагается обнаружить в переменных окружения. В файл описания виртуального хоста нужно добавить следующее

    SetEnv ARA_DB database
    SetEnv ARA_DB_HOST localhost
    SetEnv ARA_DB_PORT 3306
    SetEnv ARA_DB_LOGIN login
    SetEnv ARA_DB_PASS password

Для корректной работы cron-заданий, нужно прописать в файле .cron_profile следующее

    ARA_DB=database
    ARA_DB_HOST=localhost
    ARA_DB_PORT=3306
    ARA_DB_LOGIN=login
    ARA_DB_PASS=password
    ARA_HOME=/var/www/verkkokauppa.vhost
    
    MAILTO=gmail@gmail.com

После этого добавить задания крон. Если других заданий до этого не было, то таким образом

    cat .cron_profile cron.txt | crontab -

Для http-авторизации в файл настроек виртуального хоста нужно добавить следующее

    <Directory /var/www/virtual-host/htdocs/>
        AuthType Basic
        AuthName "Pych-pych"
        AuthUserFile /var/www/virtual-host/htdocs/.htpasswd
        require valid-user
    </Directory>

На debian-сервере сначала ставим, потом обновляем до последней версии (не помню почему именно так):

    sudo apt-get install libio-socket-ssl-perl
    sudo perl -MCPAN -e "install IO::Socket::SSL"
	
	
Отладка
-------

Так как параметры доступа хранятся в переменных окружения виртуального хоста и cron-заданий, то простой запуск скрипта парсера из командной строки эти переменные окружения не увидит.

Создаём файл .bash_profile

    export ARA_DB=database
    export ARA_DB_HOST=localhost
    export ARA_DB_PORT=3306
    export ARA_DB_LOGIN=login
    export ARA_DB_PASS=password


(похож на то, что находится в .cron_profile)

Теперь, запуская скрипт парсера таким образом, он видит нужные переменные.

    source .bash_profile; script/region-parser.pl


