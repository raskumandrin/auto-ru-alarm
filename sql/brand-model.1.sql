
CREATE TABLE `brand` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT
  ,`b_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
  ,PRIMARY KEY (`b_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `model` (
  `m_id` int(11) NOT NULL AUTO_INCREMENT
  ,`m_brand` int(11) NOT NULL
  ,`m_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
  ,PRIMARY KEY (`m_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

