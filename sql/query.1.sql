
CREATE TABLE `query` (
	`q_id` int(11) NOT NULL AUTO_INCREMENT

	,`q_active` int(11) NOT NULL

	,`q_region` int(11) NOT NULL
	,`q_city` int(11) NOT NULL
	,`q_brand` int(11) NOT NULL
	,`q_model` int(11) NOT NULL
	,`q_year_min` int(11) NOT NULL
	,`q_year_max` int(11) NOT NULL
	
	,`q_creation_time` datetime NOT NULL
	,`q_last_parser_time` datetime NOT NULL

	,PRIMARY KEY (`q_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

