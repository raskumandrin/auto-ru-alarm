
CREATE TABLE `region` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT
  ,`r_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
  ,PRIMARY KEY (`r_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `city` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT
  ,`c_region` int(11) NOT NULL
  ,`c_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL
  ,PRIMARY KEY (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

