delimiter $$

CREATE TABLE `settings` (
	`s_code` varchar(50) NOT NULL
	,`s_description` varchar(1000) NOT NULL
	,`s_value` varchar(2000) NOT NULL
	,PRIMARY KEY (`s_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8$$

delimiter ;

insert into settings (s_code,s_description,s_value) values
	('metrika','Код яндекс-метрики','');

insert into settings (s_code,s_description,s_value) values
	('host','Хост, где расположен сайт (без http, слешей)','');
