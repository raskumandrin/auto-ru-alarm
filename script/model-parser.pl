#!/usr/bin/perl -w

my $DEBUG = 0;
use Data::Dumper;

use strict;
use feature ':5.10';

use FindBin;
use lib "$FindBin::Bin/../lib/AutoRuAlarm";
use Model;

my $model = AutoRuAlarm::Model->init();
my $dbh = $model->db();

use Mojo::UserAgent;
use Mojo::JSON;
#use Mojo::CookieJar;

use Encode;
use utf8;
binmode(STDOUT,':utf8');

my %region;

my $ua = Mojo::UserAgent->new;
$ua->name('Mozilla/5.0.'.int(rand(10000)) );
# brand
my $url_brand_list = 'http://all.auto.ru';
$DEBUG and say "GET $url_brand_list";

my $jar = Mojo::UserAgent::CookieJar->new;
$jar->add(Mojo::Cookie::Response->new(name => 'adOtr', value => '38Y9F', domain => 'auto.ru', path => '/'));
$jar->add(Mojo::Cookie::Response->new(name => 'autoru_sid', value => 'ebeb9119fe940dca_22631e3aa082e378c666984f4743956b', domain => 'auto.ru', path => '/'));
$jar->add(Mojo::Cookie::Response->new(name => 'chcookie', value => '1', domain => 'auto.ru', path => '/'));
$jar->add(Mojo::Cookie::Response->new(name => 'SPSI', value => '7b3839468e608c27010b23e2e0432db3', domain => 'auto.ru', path => '/'));
$jar->add(Mojo::Cookie::Response->new(name => 'UTGv2', value => 'h44cdc681f768ccfe1416fdd53ffde97eb38', domain => 'auto.ru', path => '/'));

$ua = $ua->cookie_jar($jar);

my $tx = $ua->get( $url_brand_list );

my $brand_list_response;

if ( my $res = $tx->success ) {
	$DEBUG and say 'ok.';
	$brand_list_response = $res->body;
	$brand_list_response = decode('utf8',$brand_list_response);
}
else {
	my ($err, $code) = $tx->error;
	warn $code ? "$code response ($url_brand_list): $err" : "Connection error ($url_brand_list): $err";
}

my $brand_list_dom = Mojo::DOM->new( $brand_list_response );

#say $brand_list_response;
#exit;

$dbh->do("TRUNCATE TABLE `brand`");
$dbh->do("TRUNCATE TABLE `model`");

foreach my $brand_option_dom ( $brand_list_dom->find('select[name="mark_id"] option')->each ) {
next unless $brand_option_dom->{value};
	$dbh->do("INSERT INTO `brand` (`b_id`,`b_name`) VALUES (?,?)",undef
	  	,$brand_option_dom->{value}
		,$brand_option_dom->text
	);

	my $url_model = "http://all.auto.ru/jsexport.php?groupName=sale_search&category_id=15&section_id=1&mark_id=".$brand_option_dom->{value}."&name=mark_id";
	
	my $tx = $ua->get( $url_model );

	if ( my $res = $tx->success ) {
		$DEBUG and say 'ok.';
		my $model_response = $res->body;
		$model_response = decode('utf8',$model_response);

		my %model;
		%model = $model_response =~ /Array\('(\d+)', '([^']+)'\)/g;
	
		foreach $model (keys %model) {
			next if $model{$model} eq 'Любая';
			$dbh->do("INSERT INTO `model` (`m_id`,`m_name`,`m_brand`) VALUES (?,?,?)",undef
			  	,$model
				,$model{$model}
				,$brand_option_dom->{value}
			);
			
		}

	}
	else {
		my ($err, $code) = $tx->error;
		warn $code ? "$code response ($url_model): $err" : "Connection error ($url_model): $err";
	}
	
	say $brand_option_dom->text . $brand_option_dom->{value};

}
