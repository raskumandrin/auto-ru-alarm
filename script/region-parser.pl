#!/usr/bin/perl -w

my $DEBUG = 1;
use Data::Dumper;

use strict;
use feature ':5.10';

use FindBin;
use lib "$FindBin::Bin/../lib/AutoRuAlarm";
use Model;

my $model = AutoRuAlarm::Model->init();
my $dbh = $model->db();

use Mojo::UserAgent;
use Mojo::JSON;

use Encode;
use utf8;
binmode(STDOUT,':utf8');

my %region;

my $ua = Mojo::UserAgent->new;
#regions
my $url_region_list = 'http://auto.ru/ajax/regions/1.html';
$DEBUG and say "GET $url_region_list";

my $tx = $ua->get( $url_region_list );

if ( my $res = $tx->success ) {
	$DEBUG and say 'ok.';
	my $region_list_response = $res->body;
#	$DEBUG and say $region_list_response;

	# <li><a href='?geo=70'>\u0427\u0435\u043b \u043e\u0431\u043b.<\/a><\/li>
	%region = $region_list_response =~ /href='\?geo=(\d+)'>([^<]+)</g;
#	$DEBUG and say Dumper %region;

}
else {
	my ($err, $code) = $tx->error;
	warn $code ? "$code response ($url_region_list): $err" : "Connection error ($url_region_list): $err";
}

foreach my $region_key ( keys %region ) {
	# Переводим в юникод названия	
	# '\\u0422\\u044b\\u0432\\u0430 \\u0440\\u0435\\u0441\\u043f.';
	$region{$region_key} =~ s/\\u([[:xdigit:]]{1,4})/chr(eval("0x$1"))/egis;
#	$DEBUG and say "$region_key: $region{$region_key}";
		
	# для каждого региона делаем запрос по городам
	# http://all.auto.ru/jsexport.php?groupName=extsearch&country_id=1&region_id=2&name=region_id
		
	my $url_region = "http://all.auto.ru/jsexport.php?groupName=extsearch&country_id=1&region_id=$region_key&name=region_id";
	$DEBUG and say "GET $url_region";

	my $tx = $ua->get( $url_region );

	if ( my $res = $tx->success ) {
		$DEBUG and say 'ok.';
		my $region_response = $res->body;
		$region_response = decode('utf8',$region_response);
#		$DEBUG and say $region_response;
		
		# Вытаскиваем города по региону
		# Array('339', 'Волово')
		my %city;
		%city = $region_response =~ /Array\('(\d+)', '([^']+)'\)/g;

#		$DEBUG and say Dumper %city;
		
		$region{$region_key} = {
			name => $region{$region_key},
			city => \%city,
		}
		
	}
	else {
		my ($err, $code) = $tx->error;
		warn $code ? "$code response ($url_region): $err" : "Connection error ($url_region): $err";
	}
}

#$DEBUG and say Dumper %region;

# ВСЁ СОБРАЛИ. ПИШЕМ В БАЗУ (перед тем, всё очищаем)

$dbh->do("TRUNCATE TABLE `region`");
$dbh->do("TRUNCATE TABLE `city`");

foreach my $region_key (sort keys %region) {

	$dbh->do("INSERT INTO `region` (`r_id`,`r_name`) VALUES (?,?)",undef
	  	,$region_key
		,$region{$region_key}->{name}
	);

	foreach my $city_key (sort keys %{$region{$region_key}->{city}}) {
	
		$dbh->do("INSERT INTO `city` (`c_id`,`c_name`,`c_region`) VALUES (?,?,?)",undef
		  	,$city_key
			,${$region{$region_key}->{city}}{$city_key}
			,$region_key
		);
	}

}